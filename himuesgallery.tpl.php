<?php
/**
 * @file
 */

//variables:
// $node
// $picturelist
?>
<div class="himuesgallery_outer">

<div class="himuesgallery_text"><?php print $node->body; ?></div>
<?php
  foreach ($picturelist as $picture) {
?>
  <div class='himuesgallery_picture'>
    <div class='himuesgallery_picture_image'>
      <a href="<?php $pic = drupal_urlencode(base_path() . $node->gallerypath . "/" . $picture['File']); $pic = str_replace('%252F', '', $pic); echo $pic; ?>"
        <?php
        switch ($node->showtype) {
          case 0:
            echo "rel=lightbox[]";
            break;

          case 1:
            echo "rel=lightbox[himuesGallery]";
            break;

          default:
            echo "rel=lightshow[himuesGallery]";
        }

        echo "  title='{$picture['description']}' name='{$picture['description']}'";
        ?>
 target="_blank">
          <img src="<?php  $thumb = drupal_urlencode(base_path() . $node->gallerypath . "/thumbs/thumb." . $picture['File']);	$thumb = str_replace('%252F', '', $thumb); echo $thumb; ?>">
      </a>
    </div>
    <div class='himuesgallery_picture_description'><?php print $picture['description']; ?></div>
  </div>
<?php
  } //end of foreeach
?>
</div>
<div class="content clearfix">
</div>