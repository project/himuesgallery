<?php
/**
 * @file
 */

/**
 * This function is called before calling the tpl.php-file
 *
 * We use it to generates the thumbnails
 */
function template_preprocess_himuesgallery(&$variables) {
  $node=$variables['node'];
  $picturelist=$variables['picturelist'];

  foreach ($picturelist as $picture) {
    $file = $_SERVER['DOCUMENT_ROOT'] . base_path() . $node->gallerypath . "/" . $picture['File'];

    //check if path exist and is under sites-dir
    $file = file_create_path($file);

    //If in directory "thumbs" no file with name "thumb.<picture>" exist -> create one.
    //This only happens on first show of the node after cration or if you delete the thumb.-file.
    //This should burst the performance
    if (!file_exists(dirname($file) . "/thumbs/thumb." . $picture['File'])) _himuesgallery_create_thumb($file, $node, $picture);
  }
}


/**
 * Create the thumbnails for a new or changed gallery
 */
function _himuesgallery_create_thumb($file, $node, $picture) {

  //If thumbnail-dir dosn't exist create it
  $dir = file_create_path(dirname($file) . "/thumbs");
  if (!is_dir($dir)) mkdir($dir);

  //use the right function for file-/mime-type
  switch ($picture['type']) {
    case 1: //GIF
            $original_picture=imagecreatefromgif($file);
            break;
    case 2: //JPG
            $original_picture=imagecreatefromjpeg($file);
            break;
    case 3: //PNG
            $original_picture=imagecreatefrompng($file);
            break;
    default:
        return; //nichts tun, wenn es kein bekannter Bildtyp ist
  }

  //wide-pictures will allways be shown with wide/height from node (node get it from from css-file)
  //height-pictures will be shown with height frome node (cdd-file) and wide will be calculated
  if ($picture['width'] > $picture['height']) {
    $bild=imagecreatetruecolor($node->thumbnail_picture_width, $node->thumbnail_picture_height);
    imagecopyresized($bild, $original_picture, 0, 0, 0, 0, $node->thumbnail_picture_width, $node->thumbnail_picture_height, $picture['width'], $picture['height']);
  }
  else {
    $picture_ratio=$picture['width']/$picture['height'];
    $bild=imagecreatetruecolor($node->thumbnail_picture_height*$picture_ratio, $node->thumbnail_picture_height);
    imagecopyresized($bild, $original_picture, 0, 0, 0, 0, $node->thumbnail_picture_height*$picture_ratio, $node->thumbnail_picture_height, $picture['width'], $picture['height']);
  }

  imageinterlace($bild, 1);

  //create the thumbnail-file in thumbnail-folder
  imagejpeg($bild, dirname($file) . "/thumbs/thumb." . basename($file), $node->thumbnail_picture_quality);
  imagedestroy($original_picture);
}
